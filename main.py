#!/usr/bin/env python3
import os
import logging
import json
import time
import urllib.request

CACHE_VALUE = None
LAST_CACHE_TIME = 0
CACHE_SAVE_TIME = 3600 # 1 hour

from programaker_bridge import (
    ProgramakerBridge,
    CallbackBlockArgument,
    BlockContext,
)

bridge = ProgramakerBridge(
    name="NASA Open APIs",
    endpoint=os.environ['BRIDGE_ENDPOINT'],
    token=os.getenv('BRIDGE_AUTH_TOKEN', None),
    is_public=True,
    icon=open("logo.png", "rb"),
)

API_KEY=os.environ['API_KEY']  # Can be obtained at https://api.nasa.gov/


@bridge.callback
def get_apod_fields(_extra_data):
    return (
        {'id': 'url', 'name': 'Image thumbnail URL'},
        {'id': 'title', 'name': 'Title'},
        {'id': 'hdurl', 'name': 'HD Image URL'},
        {'id': 'explanation', 'name': 'Explanation'},
        {'id': 'date', 'name': 'Date'},
        {'id': 'copyright', 'name': 'Authorship'},
        {'id': 'json', 'name': 'JSON information'},
    )


@bridge.getter(
    id="get_apod",
    message="Get NASA's Astronomy Picture of the Day",
    arguments=[
        CallbackBlockArgument(str, get_apod_fields),
    ],
    block_result_type=str,
)
def get_apod(field, extra_data):
    global LAST_CACHE_TIME, CACHE_SAVE_TIME, CACHE_VALUE

    t = time.time()
    refresh_cache = t - LAST_CACHE_TIME > CACHE_SAVE_TIME

    logging.info("APOD: Refresh cache? {}".format(refresh_cache))
    if refresh_cache:
        result = urllib.request.urlopen(
            'https://api.nasa.gov/planetary/apod?api_key='
            + API_KEY).read()

        LAST_CACHE_TIME = t
        CACHE_VALUE = json.loads(result)

    if field == "json":
        return CACHE_VALUE
    else:
        return CACHE_VALUE[field]


if __name__ == '__main__':
   logging.basicConfig(format="%(asctime)s - %(levelname)s [%(filename)s] %(message)s")
   logging.getLogger().setLevel(logging.INFO)

   logging.info('Starting bridge')
   bridge.run()
